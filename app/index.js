const http = require('http')

http.createServer((_req, res) => {
  res.writeHead(200, {'Content-Type': 'text/plain'})
  res.end(process.env.REGION)
}).listen(3000)